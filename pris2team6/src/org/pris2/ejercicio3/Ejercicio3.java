package org.pris2.ejercicio3;

import java.util.Scanner;

public class Ejercicio3 {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Introduzca el valor del extremo de la izquierda: ");
		int xizq = in.nextInt();
		System.out.println("Introduzca el valor del extremo de la derecha: ");
		int xder = in.nextInt();
		System.out.println("El intervalo es [" + xizq + "," + xder + "]");
		System.out.println("Introduzca la precisión deseada: ");
		double precision = in.nextDouble();		

		FuncionBolzano fb = new FuncionBolzano(xizq, xder);
		
		while (fb.funcionRaiz() < precision) {
			if ((fb.funcionIzquierda() * fb.funcionRaiz()) < 0) {
				System.out.println("Vuelva a introducir el límite derecho del intervalo: ");
				fb.setXder(in.nextInt());
				System.out.println(fb.toString());
			} else if ((fb.funcionIzquierda() * fb.funcionRaiz()) > 0) {
				System.out.println("Vuelva a introducir el límite izquierdo del intervalo: ");
				fb.setXizq(in.nextInt());
				System.out.println(fb.toString());

			} else if ((fb.funcionIzquierda() * fb.funcionRaiz()) == 0) {
				System.out.println("La raíz es: " + fb.xr());
			}
		}
	}
}