package org.pris2.ejercicio3;

public class FuncionBolzano {
	private int xizq;
	private int xder;
	
	public FuncionBolzano(int xizq, int xder) {
		this.xizq = xizq;
		this.xder = xder;		
	}

	public FuncionBolzano() {
		this.xizq = 0;
		this.xder = 0;		
	}

	public Double funcionDerecha() {
		double funcionDer = triplePow(xder);
		System.out.println("f(xder)=" + funcionDer);
		return funcionDer;
	}

	public Double funcionIzquierda() {
		double funcionIzq = triplePow(xizq);
		System.out.println("f(xizquierda)=" + funcionIzq);
		return funcionIzq;
	}

	public Double xr() {
		double xr = this.xder - funcionDerecha() * (this.xizq - this.xder) / (funcionIzquierda() - funcionDerecha());
		System.out.println("xr=" + xr);
		return xr;
	}

	public Double funcionRaiz() {
		double funcionRaiz = triplePow(xr());
		System.out.println("f(xr)=" + funcionRaiz);
		return funcionRaiz;
	}

	private double triplePow(double x) {
		return Math.pow(x, 5) - Math.pow(x, 4) + Math.pow(x, 3) - 3;
	}

	public void setXizq(int xizq) {
		this.xizq = xizq;
	}

	public void setXder(int xder) {
		this.xder = xder;
	}
	
	public String toString(){
		String salida = "";
		salida += "El intervalo es [" + xizq + "," + xder + "]\n";
		salida += "f(xder)=" + this.funcionDerecha()+ "\n";
		salida += "f(xizq)=" + this.funcionIzquierda()+ "\n";
		salida += "xr=" + this.xr()+ "\n";
		salida += "f(xr)=" + this.funcionRaiz()+ "\n";
		return salida;
	}	
}
