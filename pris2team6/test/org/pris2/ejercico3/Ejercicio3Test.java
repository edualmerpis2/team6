package org.pris2.ejercico3;

import org.pris2.ejercicio3.FuncionBolzano;

import junit.framework.TestCase;

public class Ejercicio3Test extends TestCase {
	int xizq=5;
    int xder=84;   
    
    public void testEjercicio3XIzq(){    	
    	FuncionBolzano ejer=new FuncionBolzano(xizq,xder);
    	ejer.funcionIzquierda();
    	assertEquals(2622.0,ejer.funcionIzquierda());    	
    }
    
    public void testEjercicio3XDer(){    	
    	FuncionBolzano ejer=new FuncionBolzano(xizq,xder);
    	ejer.funcionDerecha();
    	assertEquals(4.132924989E9,ejer.funcionDerecha());    	
    }
    
    public void testXr(){    	    	
    	FuncionBolzano ejer=new FuncionBolzano(xizq,xder);
    	assertEquals(4.999949880984545,ejer.xr());    	
    }
    
    public void testFuncionRaiz(){    	   	
    	FuncionBolzano ejer=new FuncionBolzano(xizq,xder);    	
    	assertEquals(2621.864681459028,ejer.funcionRaiz());    	
    }
}
